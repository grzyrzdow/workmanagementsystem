<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Workers;
use App\Entity\Roles;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $createdUser = null;
        if ($request->query->get("w")) {
            $createdUser = $this->getDoctrine()->getManager()->getRepository(Workers::class)->find($request->query->get("w"));
        }
        $worker = new Workers();

        $form = $this->createFormBuilder($worker)
            ->add('cardNumber', PasswordType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'maxLength' => '10',
                    'placeholder' => 'Numer karty'
                ]
            ])
            ->add('name', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Imię'
                ]
            ])
            ->add('surname', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Nazwisko'
                ]
            ])
            ->add('username', TextType::class, [
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'Login'
                ]
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Hasła muszą być identyczne.',
                'first_options'  => [
                    'attr' => [
                        'placeholder' => 'Hasło',
                        'class' => 'form-control'
                    ]
                ],
                'second_options' => [
                    'attr' => [
                        'placeholder' => 'Powtórz hasło',
                        'class' => 'form-control'
                    ]
                ],

            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Zatwierdź',
                'attr' => [
                    'class' => 'btn btn-secondary btn-lg btn-block',
                    'maxLength' => '10',
                ]
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $worker->setPassword(
                $passwordEncoder->encodePassword(
                    $worker,
                    $form->get('password')->getData()
                )
            );
            $repository = $this->getDoctrine()->getRepository(Roles::class);
            $role = $repository->findOneBy([
                'name' => 'user',
            ]);
            $worker->setRoleId($role);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($worker);
            $entityManager->flush();

            return $this->redirectToRoute('app_register', [
                'w' => $worker->getId(),
            ]);
        }
        if ($createdUser != null)
            return $this->render('user/register.html.twig', [
                'registrationForm' => $form->createView(),
                'createdUser' => $createdUser
            ]);
        else {
            return $this->render('user/register.html.twig', [
                'registrationForm' => $form->createView(),
                'createdUser' => null
            ]);
        }
    }
}
