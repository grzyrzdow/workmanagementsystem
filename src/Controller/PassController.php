<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Workers;
use App\Entity\Pass;
use App\Service\Counters;
use App\Entity\WorkHours;

class PassController extends AbstractController
{
    /**
     * @Route("/entrance", name="app_entrance")
     */
    public function pass()
    {
        if ($this->getUser()) {
            return $this->redirect("/dashboard");
        }
        return $this->render('entrance/pass.html.twig');
    }

    /**
     * @Route("/passing/register", name="app_register_passing")
     * Method({"POST"})
     */
    public function registerPassing(Request $request): JsonResponse
    {
        $response = new JsonResponse();
        $data = $request->request->all();
        $repositoryWorkers = $this->getDoctrine()->getRepository(Workers::class);
        $repositoryWorkHours = $this->getDoctrine()->getRepository(WorkHours::class);
        $entityManager = $this->getDoctrine()->getManager();

        $workHours = $repositoryWorkHours->findAll()[0];

        $date = new \DateTime();
        $workEndDisplay = new \DateTime($date->format("Y-m-d") . "00:00:00");
        $workEnd = new \DateTime($date->format("Y-m-d") . "00:00:00");

        $workEndDisplay->setTime($workHours->getEndTimeHours() + 1, $workHours->getEndTimeMinutes());
        $workEnd->setTime($workHours->getEndTimeHours(), $workHours->getEndTimeMinutes());

        if ($data['source'] == 'card') {
            $cardNumber = $data['cardNumber'];
            $worker = $repositoryWorkers->findOneBy([
                'cardNumber' => $cardNumber,
            ]);
        } elseif ($data['source'] == 'login') {
            $login = $data['login'];
            $worker = $repositoryWorkers->findOneBy([
                'username' => $login,
            ]);
        }
        if (!$worker) {
            $response->setStatusCode(400);
            $response->setData([
                'type' => 'error',
                'header' => 'Nie znaleziono pracownika',
                'message' => 'Nie istnieje pracownik o podanych danych',
            ]);
            return $response;
        }
        if ($date > $workEnd && !$worker->getIsIn()) {
            $response->setStatusCode(400);
            $response->setData([
                'type' => 'error',
                'header' => 'Poza godzinami pracy',
                'message' => 'Wejście możliwe tylko do godziny ' . $workEnd->format('H:i') . '.',
            ]);
            return $response;
        }
        if ($date > $workEndDisplay && $worker->getIsIn()) {
            $response->setStatusCode(400);
            $response->setData([
                'type' => 'error',
                'header' => 'Poza godzinami pracy',
                'message' => 'Wyjście możliwe tylko do godziny ' . $workEndDisplay->format('H:i') . ' (Nastąpiło automatyczne wylogowanie)',
            ]);
            return $response;
        }

        $query = $entityManager->createQuery(
            'SELECT p
            FROM App\Entity\Pass p
            WHERE p.workerId = :workerId AND
            p.passedAt BETWEEN :dateFrom AND :dateTo
            AND p.didEnter = :didEnter'
        )->setParameters([
            'workerId' => $worker,
            'dateFrom' => new \DateTime($date->format("Y-m-d") . " 00:00:00"),
            'dateTo' => new \DateTime($date->format("Y-m-d") . " 23:59:59"),
            'didEnter' => true
        ]);
        $passes = $query->execute();
        $firstTime  = count($passes) == 0;
        $lateData = [
            'isLate' => false,
            'totalLate' => 0
        ];

        if ($firstTime) {
            $date->setTime($workHours->getStartTimeHours(), $workHours->getStartTimeMinutes());
            $lateData =  Counters::countLate($date->format("H:i:s"));
        }

        $pass = new Pass();
        $pass->setWorkerId($worker);
        if (new \DateTime() > $workEnd) {
            $pass->setPassedAt($workEnd);
        } else {
            $pass->setPassedAt(new \DateTime());
        }
        $worker->setIsIn(!$worker->getIsIn());
        $pass->setDidEnter($worker->getIsIn());
        $entityManager->persist($pass);

        if ($lateData['isLate']) {
            $pass->setWasLate(true);
            $pass->setlateTime($lateData['totalLate']);
        }
        $entityManager->flush();

        $response->setStatusCode(200);
        if ($worker->getIsIn()) {
            if (!$lateData['isLate']) {
                $response->setData([
                    'type' => 'success',
                    'header' => 'Zarejestrowano wejście',
                    'message' => $worker->getName() . " " . $worker->getSurname()
                ]);
            } elseif ($lateData['totalLate'] >= 60) {
                $response->setData([
                    'type' => 'warn',
                    'header' => 'Wejście' . ' (' . floor($lateData['totalLate'] / 60) . "godz " . $lateData['totalLate'] % 60 . "min spóźnienia)",
                    'message' => $worker->getName() . " " . $worker->getSurname(),
                ]);
            } else {
                $response->setData([
                    'type' => 'warn',
                    'header' => 'Wejście' . ' (' . $lateData['totalLate'] . "min spóźnienia)",
                    'message' => $worker->getName() . " " . $worker->getSurname(),
                ]);
            }
        } else
            $response->setData([
                'type' => 'success',
                'header' => 'Zarejestrowano wyjście',
                'message' => $worker->getName() . " " .  $worker->getSurname()
            ]);
        return $response;
    }

    /**
     * @Route("/passing/all_workers_out", name="app_all_workers_out")
     * Method({"POST"})
     */
    public function allWorkersOut(Request $request): JsonResponse
    {
        $response = new JsonResponse();
        $entityManager = $this->getDoctrine()->getManager();

        $repository = $this->getDoctrine()->getRepository(Workers::class);
        $repositoryWorkHours = $this->getDoctrine()->getRepository(WorkHours::class);

        $workers = $repository->findBy([
            'isIn' => true,
        ]);

        $workHours = $repositoryWorkHours->findAll()[0];
        $workEnd = new \DateTime();
        $workEnd->setTime($workHours->getEndTimeHours(), $workHours->getEndTimeMinutes());

        foreach ($workers as $worker) {
            $pass = new Pass();
            $pass->setWorkerId($worker);
            $pass->setPassedAt($workEnd);
            $worker->setIsIn(false);
            $pass->setDidEnter($worker->getIsIn());
            $pass->setWasAutomatic(true);
            $entityManager->persist($pass);
            $entityManager->flush();
        }
        $response->setStatusCode(200);
        return $response;
    }
}
