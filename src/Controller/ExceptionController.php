<?php

namespace App\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Debug\Exception\FlattenException;

class ExceptionController extends Controller
{
    public function showAction(FlattenException $exception)
    {
        if ($exception->getStatusCode() == 404)
            return $this->render('error_pages/404.html.twig', [
                'code' => 404,
                'message' => "Strona nie istnieje lub została przeniesiona"
            ]);
        elseif ($exception->getStatusCode() == 403)
            return $this->render('error_pages/403.html.twig', [
                'code' => 403,
                'message' => "Nie masz dostępu do tej strony"
            ]);
        else if ($_SERVER['APP_ENV'] == 'dev')
            return $this->render('error_pages/rest.html.twig', [
                'code' => $exception->getStatusCode(),
                'message' => $exception->getMessage()
            ]);
        else {
            return $this->render('error_pages/rest.html.twig', [
                'code' => $exception->getStatusCode(),
                'message' => "Wystąpił Nieoczekiwany błąd"
            ]);
        }
    }
}
