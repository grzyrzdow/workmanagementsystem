<?php

namespace App\Controller;

use App\Entity\WorkHours;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class SetWorkHoursController extends AbstractController
{
    /**
     * @Route("/set-work-hours", name="app_set_work_hours")
     */
    public function index(Request $request)
    {
        $hoursRepository = $this->getDoctrine()->getRepository(WorkHours::class);

        $workHoursArray = $hoursRepository->findAll();
        $workHours = $workHoursArray[0];
        $message = null;
        if ($request->query->get("success")) {
            $message = "Aktualne godziny pracy: " . $workHours->getStartTimeHours() . ":" . $workHours->getStartTimeMinutes() . " - " . $workHours->getEndTimeHours() . ":" . $workHours->getEndTimeMinutes() . ".";
        }

        $form = $this->createFormBuilder($workHours)
            ->add('startTimeHours', Numbertype::class, [
                'attr' => [
                    'class' => 'form-control time-picker',
                    'placeholder' => 'g'
                ],
                'invalid_message' => "Godzina rozpoczęcia powinna być liczbą z zakresu 0-24."
            ])
            ->add('startTimeMinutes', Numbertype::class, [
                'attr' => [
                    'class' => 'form-control time-picker',
                    'placeholder' => 'm'
                ],
                'invalid_message' => "Minuta rozpoczęcia powinna być liczbą z zakresu 0-59."
            ])
            ->add('endTimeHours', Numbertype::class, [
                'attr' => [
                    'class' => 'form-control time-picker',
                    'placeholder' => 'g'
                ],
                'invalid_message' => "Godzina zakończenia powinna być liczbą z zakresu 0-24."
            ])
            ->add('endTimeMinutes', Numbertype::class, [
                'attr' => [
                    'class' => 'form-control time-picker',
                    'placeholder' => 'm'
                ],
                'invalid_message' => "Minuta zakończenia powinna być liczbą z zakresu 0-59."
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Zapisz',
                'attr' => [
                    'class' => 'btn btn-secondary btn-lg btn-block',
                ]
            ])
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($workHours);
            $entityManager->flush();

            return $this->redirectToRoute('app_set_work_hours', [
                'success' => true,
            ]);
        }
        if ($message != null) {
            return $this->render('set_work_hours/index.html.twig', [
                'form' => $form->createView(),
                'message' => $message
            ]);
        } else {
            return $this->render('set_work_hours/index.html.twig', [
                'form' => $form->createView(),
                'message' => null
            ]);
        }
    }
}
