<?php

namespace App\Controller;

use App\Entity\Log;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class SecurityController extends AbstractController
{
    /**
     * @Route("/", name="app_card_login")
     */
    public function loginWithCard(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirect("/dashboard");
        }
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastCardNumber = $authenticationUtils->getLastUsername();

        return $this->render('security/card_login.html.twig', ['last_card_number' => $lastCardNumber, 'error' => $error]);
    }

    /**
     * @Route("/login-plain", name="app_plain_login")
     */
    public function loginPlain(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirect("/dashboard");
        }
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/plain_login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }
    /**
     * @Route("/logout", name="app_before_logout")
     */
    public function beforeLogout()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $log = new Log();
        $log->setDidLogIn(false);
        $log->setLoggedAt(new \DateTime());
        $log->setDidUseCard(null);
        $log->setWorkerId($this->getUser());

        $entityManager->persist($log);
        $entityManager->flush();
        return $this->redirect("/logout-final");
    }

    /**
     * @Route("/logout-final", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }
}
