<?php

namespace App\Controller;

use App\Entity\Workers;
use App\Entity\Pass;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;

class WorkersManagementController extends AbstractController
{
    /**
     * @Route("/workers/management", name="workers_management")
     */
    public function index(PaginatorInterface $paginator, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository(Workers::class);
        $workers = $paginator->paginate(
            $repository->findBy(
                [],
                ['surname' => "ASC"]
            ),
            $request->query->getInt('page', 1),
            20
        );
        $workersJSON = [];
        foreach ($workers as $worker) {
            array_push($workersJSON, $worker->toJSON());
        }
        return $this->render('workers_management/index.html.twig', [
            'workers' => $workers,
            'workersJSON' => $workersJSON
        ]);
    }
}
