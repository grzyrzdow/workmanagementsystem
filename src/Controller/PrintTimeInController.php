<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use App\Entity\Workers;
use App\Entity\Pass;
use App\Entity\WorkHours;
use App\Service\Counters;
use Doctrine\ORM\Query\Expr\OrderBy;

class PrintTimeInController extends AbstractController
{
    /**
     * @Route("/worker/print-time-in/{id}", name="app_print_workers_time_in")
     */
    public function index(Pdf $snappy, $id, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_SUPER_ADMIN', 403);
        $months = ['styczeń', 'luty', 'marzec', 'kwiecień', 'maj', 'czerwiec', 'lipiec', 'sierpień', 'wrzesień', 'październik', 'listopad', 'grudzień'];

        $entityManager = $this->getDoctrine()->getManager();
        $workerRepository = $this->getDoctrine()->getRepository(Workers::class);
        $workHoursRepository = $this->getDoctrine()->getRepository(WorkHours::class);
        $workHours = $workHoursRepository->findAll()[0];
        $worker = $workerRepository->find($id);
        $monthNum = $request->query->get("m");
        $monthName = $months[$monthNum - 1];


        $today = new \DateTime();
        $requested = new \DateTime();

        $year = $today->format("Y");
        $requested->setDate(1, $monthNum, 1);
        if ($today->format("m") < $requested->format("m")) {
            $year--;
        }
        $monthLength = cal_days_in_month(CAL_GREGORIAN, $monthNum, $year);

        $rangeStart = new \DateTime();
        $rangeEnd = new \DateTime();
        $rangeStart->setDate($year, $monthNum, 1);
        $rangeStart->setTime(0, 0, 0);
        $rangeEnd->setDate($year, $monthNum, $monthLength);
        $rangeStart->setTime(23, 59, 59);

        $query = $entityManager->createQuery(
            'SELECT p
            FROM App\Entity\Pass p
            WHERE p.workerId = :workerId AND
            p.passedAt BETWEEN :dateFrom AND :dateTo
            ORDER BY p.passedAt ASC'
        )->setParameters([
            'workerId' => $worker,
            'dateFrom' => $rangeStart,
            'dateTo' => $rangeEnd,
        ]);
        $passes = $query->execute();

        $today->setTime($workHours->getStartTimeHours(), $workHours->getStartTimeMinutes(), 0);
        $inWorkTimes = Counters::countWorkersTimeIn($passes, $today->format("H:i:s"));
        $totalTime = 0;
        foreach ($inWorkTimes as $e) {
            $totalTime += $e['inTime'];
        }
        for ($i = 0; $i < count($inWorkTimes); $i++) {
            $time = $inWorkTimes[$i]['inTime'];
            $inWorkTimes[$i]['inTime'] = floor($time / 60) . " godz. " . $time % 60 . " min.";
            $day = new \DateTime($inWorkTimes[$i]['day']);
            $inWorkTimes[$i]['day'] = $day->format("d");
        }
        $inWorkData = [];
        foreach ($passes as $pass) {
            if (!isset($inWorkData[$pass->getPassedAt()->format("d")]['passes']))
                $inWorkData[$pass->getPassedAt()->format("d")]['passes'] = [];
            array_push($inWorkData[$pass->getPassedAt()->format("d")]['passes'], $pass);
        }
        foreach ($inWorkTimes as $in) {
            $inWorkData[$in['day']]['times'] = $in['inTime'];
        }

        $html = $this->renderView("pdf/worker_time_in.html.twig", [
            'worker' => $worker,
            'monthName' => $monthName,
            'year' => $year,
            'periodLength' => $monthLength,
            'inWorkData' => $inWorkData,
            'totalInWorkTime' => floor($totalTime / 60) . " godz. " . $totalTime % 60 . " min."
        ]);

        return new PdfResponse(
            $snappy->getOutputFromHtml($html),
            'Wydruk'
        );

        //  return new Response(
        //    $snappy->getOutputFromHtml($html),
        //  200,
        // [
        //   'Content-Type' => 'application/pdf',
        //   ]
        //);
    }
}
