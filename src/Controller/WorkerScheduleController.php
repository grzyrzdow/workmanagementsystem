<?php

namespace App\Controller;

use App\Entity\Workers;
use App\Entity\Pass;
use App\Entity\Log;
use App\Entity\WorkHours;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\Counters;

class WorkerScheduleController extends AbstractController
{
    /**
     * @Route("/worker/schedule/{id}", name="worker_schedule")
     */
    public function index($id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $repositoryWorkers = $this->getDoctrine()->getRepository(Workers::class);
        $repositoryPasses = $this->getDoctrine()->getRepository(Pass::class);
        $repositoryLogs = $this->getDoctrine()->getRepository(Log::class);
        $repositoryWorkHours = $this->getDoctrine()->getRepository(WorkHours::class);

        $workHours = $repositoryWorkHours->findAll()[0];
        $worker = $repositoryWorkers->find($id);
        $passes = $repositoryPasses->findBy(
            [
                'workerId' => $worker
            ],
            ['passedAt' => 'ASC']
        );
        $passesJSON = [];
        foreach ($passes as $pass) {
            array_push($passesJSON, $pass->toJSON());
        }
        $workStart = new \DateTime();
        $workStart->setTime($workHours->getStartTimeHours(), $workHours->getStartTimeMinutes(), 0);
        $workerTimeIn = Counters::countWorkersTimeIn($passes, $workStart->format("H:i:s"));

        $logs = $repositoryLogs->findBy([
            'workerId' => $worker
        ]);
        $logsJSON = [];
        foreach ($logs as $log) {
            array_push($logsJSON, $log->toJSON());
        }

        return $this->render('worker_schedule/index.html.twig', [
            'passes' => $passesJSON,
            'logs' => $logsJSON,
            'workerData' => [
                'workerName' => $worker->getName(),
                'workerSurname' => $worker->getSurname(),
            ],
            'workerTimeIn' => $workerTimeIn
        ]);
    }
}
