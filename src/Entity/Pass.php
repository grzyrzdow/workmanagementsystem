<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pass
 *
 * @ORM\Table(name="pass", indexes={@ORM\Index(name="workerId", columns={"workerId"})})
 * @ORM\Entity
 */
class Pass
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="passedAt", type="datetime", nullable=false)
     */
    private $passedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="didEnter", type="boolean", nullable=false)
     */
    private $didEnter;

    /**
     * @var bool
     *
     * @ORM\Column(name="wasLate", type="boolean", nullable=false)
     */
    private $wasLate = false;

    /**
     * @var int
     *
     * @ORM\Column(name="lateTime", type="integer", nullable=false)
     */
    private $lateTime = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="wasAutomatic", type="boolean", nullable=false)
     */
    private $wasAutomatic = false;

    /**
     * @var \Workers
     *
     * @ORM\ManyToOne(targetEntity="Workers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="workerId", referencedColumnName="id")
     * })
     */
    private $workerId;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPassedAt()
    {
        return $this->passedAt;
    }

    public function setPassedAt(\DateTime $passedAt): self
    {
        $this->passedAt = $passedAt;

        return $this;
    }

    public function getDidEnter(): ?bool
    {
        return $this->didEnter;
    }

    public function setDidEnter(bool $didEnter): self
    {
        $this->didEnter = $didEnter;

        return $this;
    }

    public function getWorkerId()
    {
        return $this->workerId;
    }

    public function setWorkerId(?Workers $workerId): self
    {
        $this->workerId = $workerId;

        return $this;
    }

    public function getWasAutomatic()
    {
        return $this->wasAutomatic;
    }

    public function setWasAutomatic(bool $wasAutomatic)
    {
        $this->wasAutomatic = $wasAutomatic;

        return $this;
    }

    public function getWasLate()
    {
        return $this->wasLate;
    }

    public function setWasLate(bool $wasLate)
    {
        $this->wasLate = $wasLate;

        return $this;
    }

    public function getLateTime()
    {
        return $this->lateTime;
    }

    public function setLateTime(int $lateTime)
    {
        $this->lateTime = $lateTime;

        return $this;
    }

    public function toJSON()
    {
        return json_encode([
            'id' => $this->id,
            'passedAt' => $this->passedAt->format('Y-m-d H:i:s'),
            'didEnter' => $this->didEnter,
            'wasLate' => $this->wasLate,
            'lateTime' => $this->lateTime,
            'wasAutomatic' => $this->wasAutomatic
        ]);
    }
}
