<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Pass
 *
 * @ORM\Table(name="WorkHours")
 * @ORM\Entity
 */
class WorkHours
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="startTimeHours", type="integer", nullable=false)
     * @Assert\Range(
     * min = 0,
     * max = 24,
     * minMessage = "Godzina rozpoczęcia powinna być liczbą z zakresu 0-24.",
     * maxMessage = "Godzina rozpoczęcia powinna być liczbą z zakresu 0-24."
     * )
     * @Assert\Expression(
     *     "this.getStartTimeHours() < this.getEndTimeHours()",
     *     message = "Czas rozpoczęcia powinien być mniejszy niż czas zakończenia."
     * )
     */
    private $startTimeHours = 7;

    /**
     * @var int
     *
     * @ORM\Column(name="startTimeMinutes", type="integer", nullable=false)
     * @Assert\Range(
     * min = 0,
     * max = 59,
     * minMessage = "Minuta rozpoczęcia powinna być liczbą z zakresu 0-59.",
     * maxMessage = "Minuta rozpoczęcia powinna być liczbą z zakresu 0-59."
     * )
     */
    private $startTimeMinutes = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="endTimeHours", type="integer", nullable=false)
     * @Assert\Range(
     * min = 0,
     * max = 24,
     * minMessage = "Godzina zakończenia powinna być liczbą z zakresu 0-24.",
     * maxMessage = "Godzina zakończenia powinna być liczbą z zakresu 0-24."
     * )
     */
    private $endTimeHours = 15;

    /**
     * @var int
     *
     * @ORM\Column(name="endTimeMinutes", type="integer", nullable=false)
     * @Assert\Range(
     * min = 0,
     * max = 59,
     * minMessage = "Minuta zakończenia powinna być liczbą z zakresu 0-59.",
     * maxMessage = "Minuta zakończenia powinna być liczbą z zakresu 0-59."
     * )
     */
    private $endTimeMinutes = 0;

    /**
     * Get the value of startTimeHours
     *
     * @return  int
     */
    public function getStartTimeHours()
    {
        return $this->startTimeHours;
    }

    /**
     * Set the value of startTimeHours
     *
     * @param  int  $startTimeHours
     *
     * @return  self
     */
    public function setStartTimeHours(int $startTimeHours)
    {
        $this->startTimeHours = $startTimeHours;

        return $this;
    }

    /**
     * Get the value of startTimeMinutes
     *
     * @return  int
     */
    public function getStartTimeMinutes()
    {
        return $this->startTimeMinutes;
    }

    /**
     * Set the value of startTimeMinutes
     *
     * @param  int  $startTimeMinutes
     *
     * @return  self
     */
    public function setStartTimeMinutes(int $startTimeMinutes)
    {
        $this->startTimeMinutes = $startTimeMinutes;

        return $this;
    }

    /**
     * Get the value of endTimeHours
     *
     * @return  int
     */
    public function getEndTimeHours()
    {
        return $this->endTimeHours;
    }

    /**
     * Set the value of endTimeHours
     *
     * @param  int  $endTimeHours
     *
     * @return  self
     */
    public function setEndTimeHours(int $endTimeHours)
    {
        $this->endTimeHours = $endTimeHours;

        return $this;
    }

    /**
     * Get the value of endTimeMinutes
     *
     * @return  int
     */
    public function getEndTimeMinutes()
    {
        return $this->endTimeMinutes;
    }

    /**
     * Set the value of endTimeMinutes
     *
     * @param  int  $endTimeMinutes
     *
     * @return  self
     */
    public function setEndTimeMinutes(int $endTimeMinutes)
    {
        $this->endTimeMinutes = $endTimeMinutes;

        return $this;
    }

    /**
     * Get the value of id
     *
     * @return  int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }
}
