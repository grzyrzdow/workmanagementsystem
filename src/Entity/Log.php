<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pass
 *
 * @ORM\Table(name="Log", indexes={@ORM\Index(name="workerId", columns={"workerId"})})
 * @ORM\Entity
 */
class Log
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="loggedAt", type="datetime", nullable=false)
     */
    private $loggedAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="didLogIn", type="boolean", nullable=false)
     */
    private $didLogIn;

    /**
     * @var bool
     *
     * @ORM\Column(name="didUseCard", type="boolean", nullable=true)
     */
    private $didUseCard = 1;

    /**
     * @var \Workers
     *
     * @ORM\ManyToOne(targetEntity="Workers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="workerId", referencedColumnName="id")
     * })
     */
    private $workerId;

    /**
     * Get the value of id
     *
     * @return  int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @param  int  $id
     *
     * @return  self
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of loggedAt
     *
     * @return  \DateTime
     */
    public function getLoggedAt()
    {
        return $this->loggedAt;
    }

    /**
     * Set the value of loggedAt
     *
     * @param  \DateTime  $loggedAt
     *
     * @return  self
     */
    public function setLoggedAt(\DateTime $loggedAt)
    {
        $this->loggedAt = $loggedAt;

        return $this;
    }

    /**
     * Get the value of didLogIn
     *
     * @return  bool
     */
    public function getDidLogIn()
    {
        return $this->didLogIn;
    }

    /**
     * Set the value of didLogIn
     *
     * @param  bool  $didLogIn
     *
     * @return  self
     */
    public function setDidLogIn(bool $didLogIn)
    {
        $this->didLogIn = $didLogIn;

        return $this;
    }

    /**
     * Get the value of workerId
     *
     * @return  \Workers
     */
    public function getWorkerId()
    {
        return $this->workerId;
    }

    /**
     * Set the value of workerId
     *
     * @param  \Workers  $workerId
     *
     * @return  self
     */
    public function setWorkerId(Workers $workerId)
    {
        $this->workerId = $workerId;

        return $this;
    }

    /**
     * Get the value of didUseCard
     *
     * @return  bool
     */
    public function getDidUseCard()
    {
        return $this->didUseCard;
    }

    /**
     * Set the value of didUseCard
     *
     * @param  bool  $didUseCard
     *
     * @return  self
     */
    public function setDidUseCard($didUseCard)
    {
        $this->didUseCard = $didUseCard;

        return $this;
    }

    public function toJSON()
    {
        return json_encode([
            'id' => $this->id,
            'loggedAt' => $this->loggedAt->format('Y-m-d H:i:s'),
            'didLogIn' => $this->didLogIn,
            'didUseCard' => $this->didUseCard
        ]);
    }
}
