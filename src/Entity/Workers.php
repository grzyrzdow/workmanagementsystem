<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Roles;

/**
 * Workers
 *
 * @ORM\Table(name="workers")
 * @ORM\Entity
 * 
 * @UniqueEntity(
 *  fields="cardNumber",
 *  message="Istnieje już pracownik o podanym numerze karty."
 * )
 */
class Workers implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\Length(
     * min = 4,
     * max = 25,
     * minMessage = "Minimalna długość loginu to 4.",
     * maxMessage = "Maksymalna długość loginu to 25."
     * )
     * @ORM\Column(name="username", type="string", length=32, nullable=false)
     */
    private $username;

    /**
     * @var string
     * @Assert\Length(
     * min = 6,
     * max = 20,
     * minMessage = "Minimalna długość hasła to 6.",
     * maxMessage = "Maksymalna długość hasła to 20."
     * )
     * @ORM\Column(name="password", type="string", length=256, nullable=false)
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=64, nullable=false)
     */
    private $surname;

    /**
     * @var string
     *
     * @ORM\Column(name="cardNumber", type="string", nullable=false)
     * @Assert\Regex(
     *     pattern="/[0-9]{10}/",
     *     message="Numer karty powinien składać się tylko z cyfr i być długości 10."
     * )
     */
    private $cardNumber;

    /**
     * @var bool
     *
     * @ORM\Column(name="isIn", type="boolean", nullable=false)
     */
    private $isIn = false;


    /**
     * @var \Roles
     *
     * @ORM\ManyToOne(targetEntity="Roles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roleid", referencedColumnName="id")
     * })
     */
    private $roleId;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getCardNumber(): ?string
    {
        return $this->cardNumber;
    }

    public function setCardNumber(string $cardNumber): self
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    public function getRoles()
    {
        if ($this->roleId->getId() == 1)
            return array('ROLE_SUPER_ADMIN', 'ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER');
        elseif ($this->roleId->getId() == 2)
            return array('ROLE_ADMIN', 'ROLE_MODERATOR', 'ROLE_USER');
        elseif ($this->roleId->getId() == 3)
            return array('ROLE_MODERATOR', 'ROLE_USER');
        elseif ($this->roleId->getId() == 4)
            return array('ROLE_USER');
    }

    public function eraseCredentials()
    {
        return null;
    }

    public function getSalt()
    {
        return null;
    }


    public function getIsIn()
    {
        return $this->isIn;
    }

    public function setIsIn(bool $isIn)
    {
        $this->isIn = $isIn;

        return $this;
    }

    public function getRoleId()
    {
        return $this->roleId;
    }

    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;

        return $this;
    }
    public function toJSON()
    {
        return json_encode([
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'username' => $this->username,
            'cardNumber' => $this->cardNumber,
            'roleName' => $this->roleId->getName()
        ]);
    }
}
