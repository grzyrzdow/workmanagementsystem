<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190906101627 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE late DROP FOREIGN KEY FK_6F2A1F955AEB7AE9');
        $this->addSql('ALTER TABLE late ADD CONSTRAINT FK_6F2A1F955AEB7AE9 FOREIGN KEY (passId) REFERENCES pass (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE late DROP FOREIGN KEY FK_6F2A1F955AEB7AE9');
        $this->addSql('ALTER TABLE late ADD CONSTRAINT FK_6F2A1F955AEB7AE9 FOREIGN KEY (passId) REFERENCES pass (id)');
    }
}
