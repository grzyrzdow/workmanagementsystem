<?php

namespace App\Service;

use App\Entity\Pass;
use DateTime;

class Counters
{
    static public function countLate($workStart)
    {

        $date1 = new \DateTime();
        $start = new \DateTime($date1->format("Y-m-d") . $workStart);
        $timeDiff = $start->diff($date1);
        $hours = (int) $timeDiff->format('%r%h');
        $minutes = (int) $timeDiff->format('%r%i');
        $totalLate = $hours * 60 + $minutes;

        $data = [];

        $totalLate > 0 ? $data['isLate'] = true : $data['isLate'] = false;
        $data['totalLate'] = $totalLate;
        return $data;
    }

    static public function countWorkersTimeIn($passes, $workStart)
    {
        $iterator = 0;
        $results = [];
        while ($iterator < count($passes) - 1) {
            $dayPasses = [];
            $currentDate = $passes[$iterator]->getPassedAt()->format("Y-m-d");
            $prevDate = new \DateTime($currentDate . "00:00:00");

            if ($passes[$iterator]->getDidEnter() == false) {
                $iterator++;
            }

            for ($i = $iterator; $i < count($passes); $i++) {
                $iterator = $i;
                $newDate = $passes[$i]->getPassedAt()->format("Y-m-d");
                if ($newDate != $currentDate) {
                    break;
                } else {
                    $start = new \DateTime($workStart);
                    $date = new \DateTime($passes[$i]->getPassedAt()->format("Y:m:d H:i:s"));
                    if ($date->format("H:i:s") < $start->format("H:i:s")) {
                        $date->setTime($start->format("H"), $start->format("i"), $start->format("s"));
                        array_push($dayPasses, $date);
                    } else {
                        array_push($dayPasses, $date);
                    }
                }
            }

            if (count($dayPasses) % 2 == 1) {
                $lastIndex = count($dayPasses) - 1;
            } else {
                $lastIndex = count($dayPasses);
            }
            $inTime = 0;

            for ($i = 0; $i < $lastIndex; $i += 2) {
                $start = $dayPasses[$i];
                $end = $dayPasses[$i + 1];

                $timeDiff = $end->diff($start);
                $hours = (int) $timeDiff->format('%H');
                $minutes = (int) $timeDiff->format('%i');
                $seconds = (int) $timeDiff->format('%s');
                $inTime += 60 * $hours + $minutes + $seconds / 60;
            }
            array_push($results, [
                'day' => $prevDate->format("Y-m-d"),
                'inTime' => round($inTime, 0,  PHP_ROUND_HALF_UP),
            ]);
            unset($dayPasses);
        }
        return $results;
    }
}
