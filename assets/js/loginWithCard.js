import $ from "jquery";

//focus on card number field evey 0.5sec
document.getElementById("inputCardNumber").oninput = function() {
  $("#inputCardNumber").val(
    $("#inputCardNumber")
      .val()
      .replace(new RegExp("[^0-9]"), "")
  );
  if (document.getElementById("inputCardNumber").value.length >= 10) {
    document.getElementById("loginForm").submit();
  }
};
setInterval(function() {
  document.getElementById("inputCardNumber").focus();
}, 500);
