import $ from "jquery";
import Calendar from "tui-calendar";
import React from "react";
import ReactDOM from "react-dom";

var passesString = $("#calendar").data("passes");
var passes = new Array();
for (var i = 0; i < passesString.length; i++) {
  passes.push(JSON.parse(passesString[i]));
}
var logsString = $("#calendar").data("logs");
var logs = new Array();
for (var i = 0; i < logsString.length; i++) {
  logs.push(JSON.parse(logsString[i]));
}
var timesIn = $("#calendar").data("inTimes");

var calendar = new Calendar("#calendar", {
  defaultView: "month",
  taskView: true,
  template: {
    monthDayname: function(dayname) {
      return (
        '<span class="calendar-week-dayname-name">' + dayname.label + "</span>"
      );
    }
  }
});

$("body").click(function() {
  $(".tui-full-calendar-month-guide-block").remove();
});

//display all passing
for (var i = 0; i < passes.length; i++) {
  var pass = passes[i];
  var passingTime = pass.passedAt
    .replace(new RegExp("[0-9]{4}-[0-9]{2}-[0-9]{2} "), "")
    .substring(0, 5);

  if (pass.wasLate) {
    var lateString = pass.lateTime + "min.";
    if (pass.lateTime > 60) {
      lateString =
        Math.floor(pass.lateTime / 60) +
        "godz. " +
        (pass.lateTime % 60) +
        "min.";
    }

    calendar.createSchedules([
      {
        id: "pass " + pass.id + " late",
        title: "Spóźnienie(" + lateString + ") " + passingTime,
        color: "white",
        bgColor: "red",
        category: "time",
        dueDateClass: "",
        start: pass.passedAt
      }
    ]);
  } else if (pass.didEnter) {
    calendar.createSchedules([
      {
        id: "pass " + pass.id + " on-time",
        title: "Wejście " + passingTime,
        color: "white",
        bgColor: "#0ceb0c",
        category: "time",
        dueDateClass: "",
        start: pass.passedAt
      }
    ]);
  } else if (!pass.wasAutomatic) {
    calendar.createSchedules([
      {
        id: "pass " + pass.id + " exit",
        title: "Wyjście " + passingTime,
        color: "white",
        bgColor: "#11aa11",
        category: "time",
        dueDateClass: "",
        start: pass.passedAt
      }
    ]);
  } else if (pass.wasAutomatic) {
    calendar.createSchedules([
      {
        id: "pass " + pass.id + " automatic",
        title: "Wyjście automatyczne" + passingTime,
        color: "white",
        bgColor: "#dac50a",
        category: "time",
        dueDateClass: "",
        start: pass.passedAt
      }
    ]);
  }
}
//display all logs
for (var i = 0; i < logs.length; i++) {
  var log = logs[i];
  var logTime = log.loggedAt
    .replace(new RegExp("[0-9]{4}-[0-9]{2}-[0-9]{2} "), "")
    .substring(0, 5);

  if (log.didLogIn && log.didUseCard) {
    calendar.createSchedules([
      {
        id: "log " + log.id + " card-login",
        title: "Logowanie kartą " + logTime,
        color: "white",
        bgColor: "#3f9fe8",
        category: "time",
        dueDateClass: "",
        start: log.loggedAt
      }
    ]);
  } else if (log.didLogIn && !log.didUseCard) {
    calendar.createSchedules([
      {
        id: "log " + log.id + " plain-login",
        title: "Logowanie standardowe " + logTime,
        color: "white",
        bgColor: "#0065b3",
        category: "time",
        dueDateClass: "",
        start: log.loggedAt
      }
    ]);
  } else if (!log.didLogIn) {
    calendar.createSchedules([
      {
        id: "log " + log.id + " logout",
        title: "Wylogowanie " + logTime,
        color: "white",
        bgColor: "#aa00ff",
        category: "time",
        dueDateClass: "",
        start: log.loggedAt
      }
    ]);
  }
}

//display all times in
for (var i = 0; i < timesIn.length; i++) {
  var timeIn = timesIn[i];
  var time = "";
  if (timeIn.inTime > 60) {
    time =
      Math.floor(timeIn.inTime / 60) + "godz. " + (timeIn.inTime % 60) + "min.";
  } else {
    time = timeIn.inTime + "min.";
  }
  calendar.createSchedules([
    {
      id: "time-in",
      title: "Czas w pracy: " + time,
      bgColor: "white",
      category: "time",
      dueDateClass: "",
      start: timeIn.day
    }
  ]);
}

$("label").click(function(event) {
  calendar.changeView(
    $(event.target)
      .find(":input")
      .attr("id")
  );
  updateDateRange();
});

$("#next").click(function() {
  calendar.next();
  updateDateRange();
});
$("#prev").click(function() {
  calendar.prev();
  updateDateRange();
});

class DateRange extends React.Component {
  render() {
    var dateStart = calendar.getDateRangeStart().toDate();
    var dateEnd = calendar.getDateRangeEnd().toDate();
    if (calendar.getViewName() == "day") {
      return <h1>{dateStart.toLocaleDateString()}</h1>;
    } else {
      return (
        <h1>
          {dateStart.toLocaleDateString()} ~ {dateEnd.toLocaleDateString()}
        </h1>
      );
    }
  }
}
ReactDOM.render(<DateRange />, document.getElementById("date-range"));
function updateDateRange() {
  ReactDOM.render(<DateRange />, document.getElementById("date-range"));
}
