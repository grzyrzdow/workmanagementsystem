import $ from "jquery";

const hamburger = $(".hamburger");
const sidebar = $(".sidebar");
const nextLevel = $(".next-level");

function closeUls() {
  nextLevel.removeClass("collapsed");
  nextLevel.find("ul").removeClass("expanded");
  nextLevel.find("ul").css("max-height", "0");
}

hamburger.click(function() {
  sidebar.toggleClass("open");
  hamburger.toggleClass("open");
  closeUls();
});

sidebar.mouseleave(function() {
  if (!sidebar.hasClass("open")) {
    closeUls();
  }
});

nextLevel.click(function(event) {
  var close = false;
  if (
    $(event.target).attr("class") == "tag" ||
    $(event.target).hasClass("arrow") ||
    $(event.target).hasClass("icon")
  ) {
    var element = $(event.target)
      .parent()
      .parent();
  } else {
    var element = $(event.target).parent();
  }

  if (element.find("ul").hasClass("expanded")) {
    close = true;
  }
  var level2 = nextLevel.find("ul");
  nextLevel.removeClass("collapsed");
  level2.removeClass("expanded");
  level2.css("max-height", "0");

  if (close) {
    element.addClass("collapsed");
  }

  level2 = element.find("ul");
  var liHeight = level2
    .find("li")
    .css("height")
    .replace("px", "");
  element.toggleClass("collapsed");
  if (close) {
    level2.removeClass("expanded");
    level2.css("max-height", "0");
  } else {
    level2.addClass("expanded");
    level2.css("max-height", liHeight * level2.find("li").length + "px");
  }
});
