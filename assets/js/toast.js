import React, { Component } from "react";
import { toast, Slide, Zoom, Flip, Bounce } from "react-toastify";

//notification defaults
toast.configure({
  autoClose: 2500,
  newestOnTop: true,
  transition: Flip,
  closeButton: false
});

function notify(type, header = "Sukces", message = "") {
  if (type == "success") {
    toast.success(
      <div>
        <center>
          <b>{header}</b>
          <div>{message}</div>
        </center>
      </div>
    );
  } else if (type == "warn") {
    toast.warn(
      <div>
        <center>
          <b>{header}</b>
        </center>
        <div>{message}</div>
      </div>
    );
  } else if ((type = "error")) {
    toast.error(
      <div>
        <center>
          <b>{header}</b>
        </center>
        <div>{message}</div>
      </div>
    );
  }
}

export { notify };
