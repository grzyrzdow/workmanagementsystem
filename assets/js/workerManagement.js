import $ from "jquery";

var dataActivator = $(".worker-information-activator");
var monthActivator = $(".month-selection-activator");
var dim = $(".dim");

dim.click(function(event) {
  if ($(event.target).attr("class") == "dim") dim.css("z-index", "-100");
});

dataActivator.click(function(event) {
  var workerData = $(
    ".worker-information-container." + $(event.target).attr("id")
  ).data("worker-information");
  dim.html(
    "<div id='worker-information' class='worker-information'>" +
      "<h1>" +
      workerData.name +
      " " +
      workerData.surname +
      "</h1>" +
      "<div class='left-column'>" +
      "<div class='worker-name'>ID</div>" +
      "<div class='worker-name'>Numer karty</div>" +
      "<div class='worker-name'>Uprawnienia</div>" +
      "</div>" +
      "<div class='right-column'>" +
      "<div class='worker-name'>" +
      workerData.id +
      "</div>" +
      "<div class='worker-name'>" +
      workerData.cardNumber +
      "</div>" +
      "<div class='worker-name'>" +
      workerData.roleName +
      "</div>" +
      "</div>" +
      "</div>"
  );
  dim.css("z-index", "200");
});

monthActivator.click(function(event) {
  var workerData = $(
    ".worker-information-container." + $(event.target).attr("id")
  ).data("worker-information");
  dim.html(
    "<div class='month-select'>" +
      "<h1>Wybierz miesiąc</h1>" +
      "<div class='month-wrapper' data-simplebar>" +
      "<ul class='month-options'>" +
      "<li class='worker-information-activator'>" +
      "<a href='/worker/print-time-in/" +
      workerData.id +
      "?m=1'>Styczeń</a>" +
      "</li>" +
      "<li class='worker-information-activator'>" +
      "<a href='/worker/print-time-in/" +
      workerData.id +
      "?m=2'>Luty</a>" +
      "</li>" +
      "<li class='worker-information-activator'>" +
      "<a href='/worker/print-time-in/" +
      workerData.id +
      "?m=3'>Marzec</a>" +
      "</li>" +
      "<li class='worker-information-activator'>" +
      "<a href='/worker/print-time-in/" +
      workerData.id +
      "?m=4'>Kwiecień</a>" +
      "</li>" +
      "<li class='worker-information-activator'>" +
      "<a href='/worker/print-time-in/" +
      workerData.id +
      "?m=5'>Maj</a>" +
      "</li>" +
      "<li class='worker-information-activator'>" +
      "<a href='/worker/print-time-in/" +
      workerData.id +
      "?m=6'>Czerwiec</a>" +
      "</li>" +
      "<li class='worker-information-activator'>" +
      "<a href='/worker/print-time-in/" +
      workerData.id +
      "?m=7'>Lipiec</a>" +
      "</li>" +
      "<li class='worker-information-activator'>" +
      "<a href='/worker/print-time-in/" +
      workerData.id +
      "?m=8'>Sierpień</a>" +
      "</li>" +
      "<li class='worker-information-activator'>" +
      "<a href='/worker/print-time-in/" +
      workerData.id +
      "?m=9'>Wrzesień</a>" +
      "</li>" +
      "<li class='worker-information-activator'>" +
      "<a href='/worker/print-time-in/" +
      workerData.id +
      "?m=10'>Październik</a>" +
      "</li>" +
      "<li class='worker-information-activator'>" +
      "<a href='/worker/print-time-in/" +
      workerData.id +
      "?m=11'>Listopad</a>" +
      "</li>" +
      "<li class='worker-information-activator'>" +
      "<a href='/worker/print-time-in/" +
      workerData.id +
      "?m=12'>Grudzień</a>" +
      "</li>" +
      "</ul>" +
      "</div>" +
      "</div>"
  );
  dim.css("z-index", "200");
});
