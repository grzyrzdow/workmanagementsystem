import $ from "jquery";

var progressBar = $(".progress-bar");

var logoutTime = 190;
var currentTime = logoutTime;

$("body").mousemove(function(e) {
  progressBar.addClass("reset");
  progressBar.removeClass("reset");
  currentTime = logoutTime;
});
$("body").keypress(function(e) {
  progressBar.addClass("reset");
  progressBar.removeClass("reset");
  currentTime = logoutTime;
});

function updateCurrentTime() {
  progressBar.css("width", (currentTime * 100) / logoutTime + "%");
  currentTime -= 1;
  if (currentTime == -3) {
    window.location.href = "/logout";
  }
  setTimeout(updateCurrentTime, 300);
}

updateCurrentTime();
