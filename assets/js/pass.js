import $ from "jquery";
import { notify } from "./toast";

var requestSource;

$("#open-button").click(function() {
  document.getElementById("myForm").style.display = "block";
});

$("#cancel-button").click(function() {
  document.getElementById("myForm").style.display = "none";
});

$("#confirm-button").click(function() {
  requestSource = "login";
  registerPassing();
});

var input = document.getElementById("login");
input.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
    document.getElementById("confirm-button").click();
  }
});

document.getElementById("cardNumber").oninput = function() {
  $("#cardNumber").val(
    $("#cardNumber")
      .val()
      .replace(new RegExp("[^0-9]"), "")
  );
  if ($("#cardNumber").val().length >= 10) {
    requestSource = "card";
    registerPassing();
  }
};
//focus on card number field evey 0.5sec
setInterval(function() {
  if (document.getElementById("myForm").style.display != "block") {
    document.getElementById("cardNumber").focus();
  }
}, 500);

//register passing on 10 digits
function registerPassing() {
  $.ajax({
    type: "POST",
    url: "/passing/register",
    data: {
      source: requestSource,
      cardNumber: $("#cardNumber").val(),
      login: $("#login").val()
    },
    dataType: "json",
    success: function(response, textStatus, xhr) {
      notify(response.type, response.header, response.message);
      document.getElementById("myForm").style.display = "none";
    },
    error: function(xhr, textStatus, errorThrown) {
      notify(
        xhr.responseJSON.type,
        xhr.responseJSON.header,
        xhr.responseJSON.message
      );
    },
    complete: function() {
      $("#cardNumber").val("");
      $("#login").val("");
    }
  });
}
