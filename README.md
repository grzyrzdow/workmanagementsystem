# Work Management System made with Symfny and twig templates

Simple system that enable the boss controlling workers work time basing on the 10-digit numbers they need to provide to start the work.

**Features:**
*  starting and ending work by providing card number or login
*  viewing workers activity on a calendary
*  automatic logout after 60 without any activity
*  printing workers activity as a table in .pdf format
*  changing work hours
*  adding new workers

# Screenshots:

![screenshot-1](Screenshots/screenshot1.png)
![screenshot-2](Screenshots/screenshot2.png)
![screenshot-3](Screenshots/screenshot3.png)
![screenshot-4](Screenshots/screenshot4.png)
![screenshot-5](Screenshots/screenshot5.png)
![screenshot-6](Screenshots/screenshot6.png)
![screenshot-7](Screenshots/screenshot7.png)